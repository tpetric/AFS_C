/*!*****************************************************************************
 *******************************************************************************

 \note  AFS_structure.cpp
 \date  August 2013
 \authors: Tadej Petric

 \remarks

 Adaptive oscillator combined with the adaptive Fourier series for frequency and
 phase estimation from an input singal.

 REFERENCE: 2011 IJRR Petric et.al.

 ******************************************************************************/

#include <math.h>
#include <stddef.h>
#include <sys/time.h>
#include <string.h>

#include "AFSstructure.h"

#define pi 3.14159265358979323846

#define SQR(a) ((a)*(a))

using namespace std;


AFS_structure::AFS_structure(int dof_, int N_, double ni_, double K_)
{
int i, j, k;

  phi = NULL;
  Omega = NULL;

  y_in = NULL;
  y_fb = NULL;
    
    
  alpha = NULL;
  beta = NULL;
  
  flag = 0; 
  
  dof = dof_; N = N_;

  printf("dof = %d, N = %d\n", dof, N);

  ni = ni_; K = K_;

  printf("ni = %.2lf, K = %.2lf\n", ni, K);

  
  phi = new double[dof];
  Omega = new double[dof];
  
  y_in = new double[dof];
  y_fb = new double[dof];
    
  alpha = new double *[dof];
  beta = new double *[dof];
  for (i = 0; i < dof; i++){
    alpha[i] = new double[N];
    beta[i] = new double[N];
  }

}

AFS_structure::~AFS_structure()
{
int num;
  if (y_in != NULL)
    delete [] y_in;
  if (y_fb != NULL)
    delete [] y_fb;
  if (Omega != NULL)
    delete [] Omega;
  if (phi != NULL)
    delete [] phi;
  if (alpha != NULL || beta != NULL)
  {
    for (int i = 0; i < dof; i++){
      delete [] alpha[i];
      delete [] beta[i];
    }
    delete [] alpha;
    delete [] beta;
  }
}



void AFS_structure::AFS_param_print(void)
{
    int i, j;

    printf("\ndof = %d, N = %d, flag = %d\n", dof, N, flag);
    printf("ni = %.2lf, K = %.2lf\n", ni, K);
    
    if (phi != NULL) {
        printf("phi = (");
        for (j = 0; j < dof-1; j++)
            printf("%.2lf, ", phi[j]);
        printf("%.2lf)\n\n", phi[dof-1]);
    }
    else
        printf("phi = NULL");
 
    if (Omega != NULL) {
        printf("Omega = (");
        for (j = 0; j < dof-1; j++)
            printf("%.2lf, ", Omega[j]);
        printf("%.2lf)\n\n", Omega[dof-1]);
    }
    else
        printf("Omega = NULL");
    
    if (alpha != NULL) {
        for (j = 0; j < dof; j++){
            printf("alpha = (");
            for (i = 0; i < N-1; i++ ){
                printf("%.2lf, ", alpha[j][i]);
            }
            printf("%.2lf)\n\n", alpha[j][i]);
        }
    }
    else
        printf("alpha = NULL");

    if (beta != NULL) {
        for (j = 0; j < dof; j++){
            printf("beta = (");
            for (i = 0; i < N-1; i++ ){
                printf("%.2lf, ", beta[j][i]);
            }
            printf("%.2lf)\n\n", beta[j][i]);
        }
    }
    else
        printf("beta = NULL");
}

//static const double epsilon = 1.0e-16;

void AFS_structure::AFS_integrate(double dt)
{
    int i, j, k;
    
    double e;
    double dphi, dOmega;
    double dalpha, dbeta;
    
    const int servo_rate = 1; //SERVO_BASE_RATE / TASK_SERVO_RATIO;
    double desired_dt = 1.0 / servo_rate;
    int steps;
  
    steps = (int) (dt / desired_dt + 0.5);
  
    dt = dt / steps;

    
    // Integrate fourier series coeficients using Euler's method
    for (k = 0; k < steps; k++)
    {
        for (j = 0; j < dof; j++)
        {
            if(flag==1)   // check if learning is active !
                e = y_in[j] - y_fb[j]; // feedback error
            else
                e = 0;
            
            y_fb[j] = 0; // set to 0 for recalculation in the next loop
            
            for (i = 0; i < N; i++)
            {
                dalpha = e * ni * cos(i*phi[j]);
                alpha[j][i] += dalpha * dt;
            
                dbeta = e * ni * sin(i*phi[j]);
                beta[j][i] += dbeta * dt;
                
                beta[j][1] = 0; // this is requred otherwice the phase is not clearly defined!!!
                
                y_fb[j] += ( ( alpha[j][i] * cos(i*phi[j])) + ( beta[j][i] * sin(i* phi[j])) );
            }
            
            dphi = Omega[j] - (K * e * sin(phi[j]));
            dOmega = - K * e * sin(phi[j]);
            
            //Euler integration - new frequency and phase
            phi[j] += dphi * dt;
            Omega[j] += dOmega * dt;
        }
    }
}

void AFS_structure::set_initial_AFS_state(double frequency_)
{
    for (int j = 0; j < dof; j++)
    {
        y_fb[j] = 0;  // inital state before first calculation
        
        phi[j] = 0;
        Omega[j] = frequency_;
        
        for (int i = 0; i < N; i++)
        {
            alpha[j][i] = 0;
            beta[j][i] = 0;
        }
    }
}



double AFS_structure::get_frequency(int j)
{
  return Omega[j];
}

double AFS_structure::get_phase(int j)
{
    return phi[j];
}

double AFS_structure::get_input(int j)
{
    return y_in[j];
}

double AFS_structure::get_output(int j)
{
    return y_fb[j];
}


void AFS_structure::update_input(int i, double y_in_)
{
        y_in[i] = y_in_;
}








