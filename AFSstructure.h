
#ifndef _ADAPTIVE_AFS_H_

#define _ADAPTIVE_AFS_H_

#include <string>
// #include <time.h>
#include <fstream>
#include <sstream>
#include <iostream>

#include "utility.h"


class AFS_structure {
private:
    // Adaptive oscillar parameters
    int flag;
    int dof, N;
    double ni;
    double K;
    
    // Current state for integration
    double *phi;
    double *Omega;
  
    // Input signal
    double *y_in;
    
    // Aproximated signal (adaptive fourier series)
    double *y_fb;
    
    // Current state for integration of adaptive fourier series coeficients
    double **alpha;
    double **beta;
    
public:
    //AFS_structure(char *file_name);
    AFS_structure(int dof, int N, double ni, double K);
    ~AFS_structure();

    void AFS_param_print(void);
    void AFS_integrate(double dt);
    void set_initial_AFS_state(double frequency);
    
    void update_input(int i, double y_in_);
  
    double get_input(int i);
    double get_output(int i);
    
    double get_phase(int i);
    double get_frequency(int i);
    void set_frequency(int i, double Omega_) {Omega[i] = Omega_;};
    
    double get_ni() { return ni; };
    double get_K() { return K; };
    int get_dof() { return dof; };
    int get_N() { return N;};
    
    int get_flag() { return flag;};
    
    void set_flag(int flag_) {flag = flag_;};
    
};

#endif
